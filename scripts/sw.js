var swPrecache = require('sw-precache');
var path = require('path');

swPrecache.write(path.join('./', 'service-worker.js'), {
  verbose: true,
  staticFileGlobs: [
    './**.html',
    './img/**.png',
  ],
  runtimeCaching: [
    {
      urlPattern: /http(s)?\:\/\/localhost\/json/,
      handler: 'cacheFirst',
      options: {
        cache: {
          name: 'mock-api-cache-local',
          maxEntries: 100
        }
      }
    },
    {
      urlPattern: /http(s)?\:\/\/amp-pwa-demo\.herokuapp\.com\/json/,
      handler: 'cacheFirst',
      options: {
        cache: {
          name: 'mock-api-cache-remote',
          maxEntries: 100
        }
      }
    },
    {
      urlPattern: /http(s)?\:\/\/cdn\.ampproject\.org/,
      handler: 'cacheFirst',
      options: {
        cache: {
          name: 'amp-file-cache',
          maxEntries: 100
        }
      }
    },
    {
      urlPattern: /http(s)?\:\/\/s7d[0-9]\.scene7\.com/,
      handler: 'cacheFirst',
      options: {
        cache: {
          name: 'scene7-cache',
          maxEntries: 50
        }
      }
    },
    {
      urlPattern: /http(s)?\:\/\/amppwa\.herokuapp\.com/,
      handler: 'cacheFirst',
      options: {
        cache: {
          name: 'api-cache',
          maxEntries: 100
        }
      }
    }
  ]
});
