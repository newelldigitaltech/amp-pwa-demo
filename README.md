# PWA/AMP Demo

### Web App Manifest
[MDN Specs](https://developer.mozilla.org/en-US/docs/Web/Manifest)

### Service Workers
[Introduction](https://developers.google.com/web/ilt/pwa/introduction-to-service-worker)

[sw-precache](https://github.com/GoogleChromeLabs/sw-precache)

[sw-toolbox](https://github.com/GoogleChromeLabs/sw-toolbox)

### AMP
[Amp by Example](https://ampbyexample.com)
